module.exports = { 
  "env": { 
    "browser": true, 
    "node": true, 
    "jasmine": true 
  }, 
  "extends": "eslint:recommended", 
  "parserOptions": { 
    "ecmaVersion": 5 
  }, 
  "rules": { 
    "indent": [ 
      "error", 
      "tab" 
    ], 
    "linebreak-style": [ 
       "error", 
       "windows" 
    ], 
    "quotes": [ 
       "error", 
       "single" 
    ], 
    "semi": [ 
       "error", 
       "always" 
    ] 
  } 
}; 
