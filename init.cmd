::::::::::::::::::::::::: INIT.CMD :::::::::::::::::::::::::

:: Script to set up a blank install of a web app
:: Gabe Buckley
:: gb@gabrielbuckley.com
:: v1.0.0
:: 17/08/2018

:: TO DO ::

:: :: :: HOUSEKEEPING :: :: ::

  :: Turn off console output
  @ECHO OFF

:: Set flags to 0 to switch off

  :: Debug Flag
  SET /A debug=0

:: Re-nable console output for debug mode
  IF /I "%debug%" NEQ "0" @ECHO ON

:: :: :: CONSTANTS :: :: ::

SET /A ALL_OK=0
SET /A DEBUG_MODE_ON=%debug%

:: :: :: DEFAULTS :: :: ::

SET default_name="Gabe Buckley"

SET default_email="gb@gabrielbuckley.com"

SET default_app_name="blanc"

SET default_app_version="1.0.0"

CALL :dequote default_name %default_name%
CALL :dequote default_email %default_email%
CALL :dequote default_app_name %default_app_name%
CALL :dequote default_app_version %default_app_version%

:: :: :: WELCOME MESSAGE :: :: ::
ECHO *** Welcome to [BLANC]! ***
ECHO.
ECHO.

:: :: :: GET INPUT VALUES :: :: ::

:get_name
SET /P name=[BLANC] Please enter your name (ENTER to use default: %default_name%): 
IF "%name%"=="" SET name=%default_name%

:get_email
SET /P email=[BLANC] Please enter your email (ENTER to use default: %default_email%): 
IF "%email%"=="" SET email=%default_email%

:get_app_name
SET /P app_name=[BLANC] Please enter your app_name (ENTER to use default: %default_app_name%): 
IF "%app_name%"=="" SET app_name=%default_app_name%

:get_app_version
SET /P app_version=[BLANC] Please enter your app_version (ENTER to use default: %default_app_version%): 
IF "%app_version%"=="" SET app_version=%default_app_version%

:set_system_vars
SET safe_app_name=%app_name: =_%
SET npm_pkg_name=%safe_app_name:_=-%


ECHO.
ECHO [BLANC] Current Application Variables
ECHO ---------------------------------------------
ECHO ^| App Name     ^| %app_name%     
ECHO ^| App Version  ^| %app_version% 
ECHO ^| Author Name  ^| %name%       
ECHO ^| Author Email ^| %email%
ECHO ---------------------------------------------
ECHO.

SET /P do_continue=[BLANC] If the above are correct, press ^<ENTER^> to continue, N to correct: 

IF NOT "%do_continue%"=="" GOTO get_name

:display_system_vars
ECHO.
ECHO [BLANC] Current System Variables
ECHO ---------------------------------------------
ECHO ^| Safe App Name    ^| %safe_app_name%     
ECHO ^| NPM Package Name ^| %npm_pkg_name% 
ECHO ---------------------------------------------
ECHO.

SET /P do_continue=[BLANC] To create your app using these variables, press ^<ENTER^> to continue, N to correct:

IF "%do_continue%"=="" GOTO create_dir_tree

:read_system_vars

SET /P safe_app_name=[BLANC] Enter your safe application name ( alpha / underscore only, eg my_app ):

SET /P npm_pkg_name=[BLANC] Enter your NPM package name ( alpha / dash only, eg my-app ):

GOTO display_system_vars

:: :: :: CREATE DIRECTORY STRUCTURE :: :: ::
:create_dir_tree

MKDIR dev

MKDIR dev\less
MKDIR dev\js
MKDIR dev\res
MKDIR dev\templates

MKDIR pub
MKDIR pub\assets
MKDIR pub\assets\js
MKDIR pub\assets\css
MKDIR pub\assets\img
MKDIR pub\assets\data
MKDIR pub\assets\templates

MKDIR dev\api
MKDIR pub\api

MKDIR test
MKDIR test\helpers
MKDIR test\tests

:: :: :: CREATE INITIAL FILES :: :: ::

CALL :config_jasmine

CALL :make_html

CALL :make_js

CALL :make_less

CALL :make_readme

:: :: :: INITIALISE LOCAL GIT REPO :: :: ::

:: Add ignore paterns to .gitignore file
ECHO init.cmd > .gitignore
ECHO cleanup.cmd >> .gitignore
ECHO node_modules/ >> .gitignore
ECHO pub/ >> .gitignore

:: Initialise git repo with the created files saved as the initial commit
git init
git add .
git commit -m "Initial Commit"


:: :: :: INITIALISE NPM :: :: ::

:: Initialise Node Package Manager
npm update -D

GOTO :EOF

:outro
IF /I "%debug%" NEQ "0" EXIT ALL_OK

EXIT DEBUG_MODE_ON



:: :: :: SUBROUTINES :: :: ::

:dequote
  SET %~1=%~2
  EXIT /b
  
:setdefault
  SET %~1=%~2
  EXIT /b

:config_jasmine

:: Tell Jasmine where to find the config
  SETX JASMINE_CONFIG_PATH ./jasmine.json

  EXIT /b
  

:make_html
  :: INDEX.HTML

  ECHO ^<html^> > .\dev\index.html
  ECHO  ^<head^> >> .\dev\index.html
  ECHO   ^<title^>%app_name%^</title^> >> .\dev\index.html
  ECHO. >> .\dev\index.html
  ECHO   ^<link rel=^"stylesheet^" href=^"./assets/css/site.css^" type=^"text/css^" /^> >> .\dev\index.html
  ECHO   ^<script type=^"text/javascript^" language=^"Javascript^" src=^"./assets/js/%safe_app_name%.js^"^>^</script^> >> .\dev\index.html
  ECHO. >> .\dev\index.html
  ECHO   ^<meta http-equiv=^"content-language^" content=^"en-AU^"^> >> .\dev\index.html
  ECHO   ^<meta charset=^"UTF-8^"^> >> .\dev\index.html
  ECHO   ^<meta name=^"author^" content=^"%name%, %email%^"^> >> .\dev\index.html
  ECHO  ^</head^> >> .\dev\index.html
  ECHO  ^<body^> >> .\dev\index.html
  ECHO   ^<!-- Your Code Goes Here --^> >> .\dev\index.html
  ECHO   ^<script src="//localhost:35729/livereload.js"^>^</script^> >> .\dev\index.html
  ECHO  ^</body^> >> .\dev\index.html
  ECHO ^</html^> >> .\dev\index.html

  EXIT /b

:make_js
  :: APP_NAME.JS

  ECHO /*****************************************************  > .\dev\js\%safe_app_name%.js
  ECHO   %safe_app_name%.js  >> .\dev\js\%safe_app_name%.js
  ECHO.   >> .\dev\js\%safe_app_name%.js
  ECHO   @version: %app_version%  >> .\dev\js\%safe_app_name%.js
  ECHO   @author: %name%  >> .\dev\js\%safe_app_name%.js
  ECHO   @email:  %email%  >> .\dev\js\%safe_app_name%.js
  ECHO *****************************************************/  >> .\dev\js\%safe_app_name%.js
  ECHO.   >> .\dev\js\%safe_app_name%.js
  ECHO if (window.global == null) {  >> .\dev\js\%safe_app_name%.js
  ECHO   window.global = {};  >> .\dev\js\%safe_app_name%.js
  ECHO }  >> .\dev\js\%safe_app_name%.js
  ECHO.   >> .\dev\js\%safe_app_name%.js
  ECHO window.global.%safe_app_name% = {  >> .\dev\js\%safe_app_name%.js
  ECHO   app: {  >> .\dev\js\%safe_app_name%.js
  ECHO     init: function () {  >> .\dev\js\%safe_app_name%.js
  ECHO       alert('%app_name% is alive!');  >> .\dev\js\%safe_app_name%.js
  ECHO       // Delete the above line and replace with your  >> .\dev\js\%safe_app_name%.js
  ECHO       // own initialistation code  >> .\dev\js\%safe_app_name%.js
  ECHO     }  >> .\dev\js\%safe_app_name%.js
  ECHO   },  >> .\dev\js\%safe_app_name%.js
  ECHO   data: {},  >> .\dev\js\%safe_app_name%.js
  ECHO   ui: {},  >> .\dev\js\%safe_app_name%.js
  ECHO   util: {}  >> .\dev\js\%safe_app_name%.js
  ECHO }  >> .\dev\js\%safe_app_name%.js
  ECHO.   >> .\dev\js\%safe_app_name%.js
  ECHO var _%safe_app_name% = window.global.%safe_app_name%;  >> .\dev\js\%safe_app_name%.js
  ECHO.   >> .\dev\js\%safe_app_name%.js
  ECHO window.addEventListener('load', _%safe_app_name%.app.init);  >> .\dev\js\%safe_app_name%.js
  ECHO.   >> .\dev\js\%safe_app_name%.js
  ECHO //EOF  >> .\dev\js\%safe_app_name%.js
  ECHO.   >> .\dev\js\%safe_app_name%.js
  
  
  :: api/index.js
  ECHO  var express = require('express'); > .\dev\api\index.js
  ECHO  var %safe_app_name% = express(); >> .\dev\api\index.js
  ECHO. >> .\dev\api\index.js
  ECHO  %safe_app_name%.get('/', function (req, res) { >> .\dev\api\index.js
  ECHO    var objAppDetails = { >> .\dev\api\index.js
  ECHO      "name": "%safe_app_name%" >> .\dev\api\index.js
  ECHO    } >> .\dev\api\index.js
  ECHO    res.send(objAppDetails); >> .\dev\api\index.js
  ECHO  }); >> .\dev\api\index.js
  ECHO.  >> .\dev\api\index.js
  ECHO  var server = %safe_app_name%.listen(3030, function () { >> .\dev\api\index.js
  ECHO    var host = server.address().address; >> .\dev\api\index.js
  ECHO    var port = server.address().port; >> .\dev\api\index.js
  ECHO    console.log("Example app listening at http://%s:%s", host, port); >> .\dev\api\index.js
  ECHO  }); >> .\dev\api\index.js
  ECHO.  >> .\dev\api\index.js
  
  EXIT /b

:make_less
  :: APP_NAME.LESS

  ECHO /*****************************************************  > .\dev\less\%safe_app_name%.less
  ECHO   %safe_app_name%.less  >> .\dev\less\%safe_app_name%.less
  ECHO.   >> .\dev\less\%safe_app_name%.less
  ECHO   @version: %app_version%  >> .\dev\less\%safe_app_name%.less
  ECHO   @author: %name%  >> .\dev\less\%safe_app_name%.less
  ECHO   @email:  %email%  >> .\dev\less\%safe_app_name%.less
  ECHO *****************************************************/  >> .\dev\less\%safe_app_name%.less  
  ECHO.   >> .\dev\less\%safe_app_name%.less
  ECHO html,body{ >> .\dev\less\%safe_app_name%.less
  ECHO   margin: 0; >> .\dev\less\%safe_app_name%.less
  ECHO   padding: 0; >> .\dev\less\%safe_app_name%.less
  ECHO   font-family: sans-serif; >> .\dev\less\%safe_app_name%.less
  ECHO } >> .\dev\less\%safe_app_name%.less
  ECHO. >> .\dev\less\%safe_app_name%.less
  ECHO /* EOF */ >> .\dev\less\%safe_app_name%.less
  ECHO. >> .\dev\less\%safe_app_name%.less
  
  :: main.less
  ECHO /** Import the Suite of Styles **/ > .\dev\less\main.less
  ECHO. >> .\dev\less\main.less
  ECHO @import "normalise.less"; >> .\dev\less\main.less
  ECHO @import "%safe_app_name%.less"; >> .\dev\less\main.less
  ECHO. >> .\dev\less\main.less
  ECHO /* EOF */ >> .\dev\less\main.less
  ECHO. >> .\dev\less\main.less
  
  EXIT /b
  
:make_readme
  :: README.MD
  ECHO %app_name% readme.md > readme.md
  ECHO ======= >> readme.md
  ECHO Author: %name% >> readme.md
  ECHO Email: %email% >> readme.md
  ECHO. >> readme.md
  ECHO Enter your readme notes here >> readme.md
  ECHO. >> readme.md
  ECHO ## Installation >> readme.md
  ECHO Enter installation instructions here >> readme.md
  Echo.  >> readme.md
  EXIT /b
:: END
:EOF