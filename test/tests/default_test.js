describe("Default Test Suite", function () {
	// Set up any shared variables here
	var foo = 0;

	// beforeAll is run prior to any test execution taking place
	beforeAll(function () {
		foo = 1;
	});

	// beforeEach is run prior to each test execution taking place
	beforeEach(function () {
		foo += 1;
	});

	// afterEach is run after each test execution takes place
	afterEach(function () {
		foo = 0;
	});

	// afterAll is run after all test execution has taken place
	afterAll(function () {
		foo = 0;
	});

	// it functions are the individual tests that comprise the suite
	it("Is Jasmine working?", function () {
		expect(foo).toEqual(2); // Set to 1 in beforeAll, +=1 in beforeEach = 2
	});

});
