// GRUNT Automation Tool Configuration File 
module.exports = function (grunt) {
	var LIVERELOAD_PORT = 35729;
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		connect: {
			server: {
				options: {
					base: 'dev',
					livereload: LIVERELOAD_PORT
				}
			}
		},

		watch: {
			options: {
				livereload: LIVERELOAD_PORT,
			},
			css: {
				files: ['dev/less/**/*.less'],
				tasks: ['less'],
			},
		},

		less: {
			development: {
				options: {
					paths: ['./dev/less']
				},
				files: {
					'./pub/assets/css/site.css': './dev/less/main.less'
				}
			}
		},

	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-connect');

	grunt.registerTask('server', [
    'connect',
    'watch'
  ]);
}
